<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/', 'PageController@homePage')->name('page.home');
// Route::get('/support', 'PageController@supportPage')->name('page.support');
// Route::get('/news-event', 'PageController@newsEventPage')->name('page.news');
// Route::get('/application', 'PageController@applicationPage')->name('page.application');
// Route::get('/reset-password/{hash}', 'PageController@resetPasswordPage')->name('page.reset_password');
// Route::get('/article', 'PageController@articlePage')->name('page.article');
// Route::get('/profile', 'PageController@profilePage')->name('page.profile');
// Route::get('/job/detail', 'PageController@jobDetailPage')->name('page.job_detail');
// Route::get('/jobs', 'PageController@jobPage')->name('page.jobs');
// Route::get('/career-advice', 'PageController@careerPage')->name('page.career');
// Route::get('/privacy-policy', 'PageController@privacyPolicyPage')->name('page.privacy');
// Route::get('/tips1', 'PageController@tips1Page')->name('page.tips1');
// Route::get('/tips2', 'PageController@tips2Page')->name('page.tips2');
// Route::get('/tips3', 'PageController@tips3Page')->name('page.tips3');
// Route::get('/tips4', 'PageController@tips4Page')->name('page.tips4');
// Route::get('/tips5', 'PageController@tips5Page')->name('page.tips5');
// Route::get('/tips6', 'PageController@tips6Page')->name('page.tips6');
// // Route::get('/tips/{$id}', ['as'=> 'page.tips', 'uses' => 'PageController@tipsPage']);
// Route::get('/internal-hiring', 'PageController@hiringPage')->name('page.hiring');
// Route::get('/contact', 'PageController@contactPage')->name('page.contact');
// Route::get('/application-detail/{id}', 'PageController@applicationDetailPage')->name('page.application_detail');
// Route::get('/edit-resume', 'PageController@editResumePage')->name('page.edit_resume');
// Route::get('lang/{locale}', 'PageController@setLocale')->where('locale', 'jp|en|id')->name('page.locale');

