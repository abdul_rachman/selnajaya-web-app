@extends('layouts.main', ['page' => 'page-home'])

@section('title', 'OS Selnajaya')

@section('content')
  @include('layouts.header')

  <section id="home-landing" class="home-landing">    
    <div class="landing--slider">
      <div id="landing-slider" class="h-100">
        @foreach([1,2,3] as $s)
          <div class="slider-item h-100">
            <div class="slider-image" style="background-image: url('{{ asset('images/landing-image-'.$s.'.jpg') }}');"></div>

            <div class="container">

            </div>
          </div>
        @endforeach
      </div>

      <div class="slider-content w-100 position-absolute" style="top: 0; left: 0;">
        <div class="text-center text-white">
          {{-- <h3><span>Unlock Your</span><br> <b>POTENTIALS</b></h3> --}}
          <h3><span>A job isn't just a job,<br> </span><b>It's who YOU ARE</b></h3>
          
          <div class="mt-5">
            <a href="#login" class="btn btn-white btn-bold mx-2 popup" id="login-home">@lang('general.login')</a>
            <a href="#register" class="btn btn-orange btn-bold mx-2 popup" id="register-home">@lang('general.register')</a>
          </div>
        </div>
      </div>

      <div class="landing--dots">
        @foreach([1,2,3] as $s)
          <span data-index="{{ $loop->index }}"></span>
        @endforeach
      </div>
    </div>

    <div id="vue-filter-vacancy-app">
      <filter-vacancy
        gotojobs="{{route('page.jobs')}}"
        setlang={{Config::get('app.locale')}}
        urlapi={{env('API_URL')}}
      ></filter-vacancy>
    </div>
  </section>

  <section id="home-vacancies" class="home-vacancies">
    @php
      $lang = Config::get('app.locale');
      if($lang == 'en'){
        $imgVacancy = 'png';
      } else {
        $imgVacancy = 'svg';
      }
    @endphp
    <div class="container d-flex">
      <div class="vacancy--content">
      <h2><b>@lang('general.welcome')</b> @lang('general.app_name')</h2>

        <div class="content-text">
          <p class="text-lead text-primary"><b>@lang('general.home_info_1')</b></p>
          <p>@lang('general.home_info_2')</p>
        </div>

        <div class="row">
          <div class="col-lg-4">
            <div class="content-item">
              <img src="{{ asset('images/vacancy-item-1.'.$imgVacancy) }}" alt=""/>
              <h3>@lang('general.home_info_3')</h3>
              <p>@lang('general.home_info_4')</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="content-item">
              <img src="{{ asset('images/vacancy-item-2.'.$imgVacancy) }}" alt=""/>
              <h3>@lang('general.home_info_5')</h3>
              <p>@lang('general.home_info_6')</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="content-item">
              <img src="{{ asset('images/vacancy-item-3.'.$imgVacancy) }}" alt=""/>
              <h3>@lang('general.home_info_7')</h3>
              <p>@lang('general.home_info_8')</p>
            </div>
          </div>
        </div>
      </div>

      {{-- <div class="vacancy--news">
        <h3><b>Latest News</b> &amp; Event</h3>

        <div class="news">
          <div class="news-item">
            <img src="{{ asset('images/landing-image.jpg') }}" alt=""/>
            <span>AUGUST 09 / 2018</span>
            <h4><a href="{{ route('page.article') }}">Digital Growth More on Business ...</a></h4>
          </div>

          <div class="news-item">
            <img src="{{ asset('images/landing-image.jpg') }}" alt=""/>
            <span>AUGUST 09 / 2018</span>
            <h4><a href="{{ route('page.article') }}">Digital Growth More on Business ...</a></h4>
          </div>

          <div class="news-item">
            <img src="{{ asset('images/landing-image.jpg') }}" alt=""/>
            <span>AUGUST 09 / 2018</span>
            <h4><a href="{{ route('page.article') }}">Digital Growth More on Business ...</a></h4>
          </div>
        </div>

        <div class="news-more">
          <a href="{{ route('page.news') }}"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right" style="margin-left: -4px;"></i> <span>See More</span></a>
        </div>
      </div> --}}
      <div id="vue-article-home">
        <article-home 
          landingimg={{ asset('images/landing-image.jpg') }}
          urldetail="{{ route('page.article') }}"
          urlnews={{route('page.news')}}
          urlapi="{{env('API_URL')}}"
          setlang={{Config::get('app.locale')}}
          ></article-home>
      </div>
    </div>
  </section>
  @php
    $language = Config::get('app.locale');
    if($language == 'jp'){
      $slides = ['熊谷優花', '若林千佳', '青柳智子', '高橋智子'];
    } else {
      $slides = ['Sulthon', 'Rio', 'Vita', 'Ridho', 'Ria', 'Stephanus', 'Lintang', 'Juanda', 'Herlys', 'Lilis'];
    }
  @endphp

  <div class="divpage-application-consultant">
    <div id="vue-application-consultant-app">
      <application-consultant 
        urlapi="{{env('API_URL')}}"
        setlang={{Config::get('app.locale')}}
        imgwa={{ asset('images/icon_btn_wa.png') }}
        imgmail={{ asset('images/icon-mail.png') }}
        ></application-consultant>
    </div>
  </div>

  {{-- <section id="home-consultants" class="home-consultants">
    <div class="container">
      <h3 class="section-header text-purple"><b>@lang('general.our_consultant')</b></h3>
      <p class="text-center section-description">@lang('general.info_consultant')</p>

      <div class="consultant--slider">
        <div id="slider-consultant">
          @php
            $language = Config::get('app.locale');
            if($language == 'jp'){
              $slides = ['熊谷優花', '若林千佳', '青柳智子', '高橋智子'];
              $show = ['熊谷優花', '若林千佳', '青柳智子', '高橋智子'];
            } else {
              $slides = ['Sulthon', 'Rio', 'Vita', 'Ridho', 'Ria', 'Stephanus', 'Lintang', 'Juanda', 'Herlys', 'Lilis'];
              $show = ['Sulthon', 'Rio', 'Vita', 'Ria', 'Herlys'];
            }
          @endphp

          @foreach ($slides as $slide)
            @if(in_array($slide, $show))
            <div class="slider-item">
              @php
                  $idx = $loop->iteration <= 3 ? $loop->iteration : ($loop->iteration - 3);
              @endphp

              <img src="{{ asset('images/consultant-' . $slide . '.jpg') }}" alt="{{ $slide }}"/>
              <h5 class="text-uppercase">{{ $slide }}</h5>
              <span>@lang('general.job_position')</span>
              <div style="{{$language == 'jp' ? 'height:250px' : 'height:190px'}}">@lang('general.'.$slide)</div>
            </div>
            @endif
          @endforeach
        </div>

        <span class="slider-nav-prev"><i class="fas fa-angle-left"></i></span>
        <span class="slider-nav-next"><i class="fas fa-angle-right"></i></span>
      </div>

      <div class="text-center mt-5">
        @if($language == 'jp')
            <a href="https://mail.google.com/mail/?view=cm&fs=1&to=Japan-desk@os-selnajaya.com&su=SUBJECT&body=BODY" target="_blank" class="btn-mail btn-round-oval btn-blue-sp">@lang('general.send_email')<span><img class="icon-mail-float" src="{{ asset('images/icon-mail.png') }}"/><span/></a>
        @else
            <a href="https://web.whatsapp.com/send?phone=6281284568300" target="_blank" class="btn btn-round btn-white"><span><img class="icon-wa" src="{{ asset('images/icon-wa.png') }}"/></span>WhatsApp</a>
            <a href="https://web.whatsapp.com/send?phone=6281284568300" target="_blank" class="btn btn-green-wa text-white"><span>Talk To Us</span><span><img class="icon-wa" src="{{ asset('images/wa-white.png') }}"/></span></a>
        @endif
      </div>
    </div>
  </section> --}}

  <section id="home-steps" class="home-steps" style="background-image: url('{{ asset('images/home-steps-new.jpg') }}');">
    <div class="container">
      <h3 class="section-header text-blue"><b>@lang('general.our_service')</b></h3>

      <div class="step--steps">
        @foreach ([1, 2, 3, 4] as $n)
          <div class="steps-indicator" data-index="{{ $loop->index }}">
            <span>{{ $n }}</span>
            <h6>@lang('general.step_'.$n)</h6>
          </div>
        @endforeach
      </div>

      <div class="step--slides">
        <div class="slides-slider">
          <div id="slider-steps">
            @foreach ([1, 2, 3, 4] as $n)
              <div class="slide-item">
                <div class="item-image">
                  <img src="{{ asset('images/slide-step-'.$n.'.jpg') }}" alt=""/>
                </div>
                <h4>@lang('general.title_step_'.$n)</h4>
                <p>@lang('general.content_step_'.$n)</p>
              </div>
            @endforeach
          </div>

          <span class="slider-nav-prev"><i class="fas fa-chevron-left"></i></span>
          <span class="slider-nav-next"><i class="fas fa-chevron-right"></i></span>
        </div>
      </div>
    </div>
  </section>

  <section id="home-jobs" class="home-jobs">
    {{-- <div class="container">
      <h3 class="section-header text-blue"><b>@lang('general.latest_vacancies')</b></h3>

      <p class="section-description text-center">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br> "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>

      <div class="d-flex justify-content-center w-75 mx-auto pt-5">
        <div class="legend-item"><span class="item-logo dollar mr-3"></span>@lang('general.high_position')</div>
        <div class="legend-item"><span class="item-logo fg mr-3"></span>@lang('general.fresh')</div>
        <div class="legend-item"><span class="item-logo noexp mr-3"></span>@lang('general.no_experience')</div>
      </div>

      <div class="job--vacancies">
        @php
          $vacancies = ['Sales Manager', 'Finance Staff', 'UI Developer', 'Estimator Marketing', 'Customer Service Supervisor', 'Senior Java Developer'];
        @endphp

        <div class="row">
          @foreach ($vacancies as $vacancy)
            <div class="col-lg-4">
              <a href="{{ route('page.job_detail') }}" class="vacancy-item">
                <h4 class="item-title">
                  {{ $vacancy }}

                  @if ($loop->index === 0)
                    <span class="item-logo dollar"></span>
                  @elseif ($loop->index === 1)
                    <span class="item-logo fg"></span>
                  @elseif ($loop->last)
                    <span class="item-logo noexp"></span>
                  @endif
                </h4>
                <div class="item-location"><i class="fas fa-fw fa-map-marker"></i> Jakarta Selatan, Indonesia</div>
                <div class="item-salary"><i class="fas fa-fw fa-money-bill"></i> Rp. 6.000.000 - Rp. 8.000.000</div>
                <div class="item-time"><i class="fas fa-fw fa-clock"></i> Posting time</div>
              </a>
            </div>
          @endforeach
        </div>
      </div> --}}

      <div class="divpage-application-job">
        <div id="vue-application-job-app">
          <application-job 
            detail="{{route('page.job_detail')}}"
            urlapi="{{env('API_URL')}}"
            setlang={{Config::get('app.locale')}}
            execicon={{ asset('images/exec.png') }}
            fgicon={{ asset('images/fg.png') }}
            jpsicon={{ asset('images/jps.png') }}
            ></application-job>
        </div>
      </div>

      <div class="text-center">
        <a href="{{ route('page.jobs') }}" class="btn btn-blue btn-round btn-fwl">@lang('general.see_all')</a>
      </div>
    </div>
  </section>

  <div class="divpage-application-testimony">
    <div id="vue-application-testimony-app">
      <application-testimony 
        urlapi="{{env('API_URL')}}"
        setlang={{Config::get('app.locale')}}
        imgwa={{ asset('images/wa-white.png') }}
        imgmail={{ asset('images/icon-mail.png') }}
        ></application-testimony>
    </div>
  </div>
  {{-- @if($language == 'jp')
  <section id="home-testimony" class="home-testimony">
    <div class="container">
      <h3 class="section-header text-purple"><b>@lang('general.testimony')</b></h3>

      <div class="testimony--slider">
        <div id="slider-testimony">
          @foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] as $n)
            @php
              
              if($n == 1 || $n == 2 || $n == 3 || $n == 4 || $n == 5){
                $profile = 'images/profile-cw-1.png';
              } else if($n == 6 || $n == 7 || $n == 8 || $n == 9 || $n == 10){
                $profile = 'images/profile-2.png';
              } else if($n == 11 || $n == 12 || $n == 13 || $n == 14 || $n == 15){
                $profile = 'images/profile-3.png';
              } else {
                $profile = 'images/profile-1.png';
              } 
            @endphp
            <div class="slider-item">
              <img src="{{ asset($profile) }}" alt="Testimony {{ $n }}"/>
              <h5>@lang('general.gender_testimony_'.$n)</h5>
              <h4>@lang('general.criteria_testimony_'.$n)</h4>
              <h3>@lang('general.testi_'.$n)</h3>
              <div>
                <div
                style="overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-box-orient: vertical;
                -webkit-line-clamp: 5;
                line-height: 1.5em;        
                max-height: X*N; "
                >
                  @lang('general.content_testi_'.$n)  
                </div>
                <a href="#testimony-popup-{{ $loop->index }}" class="text-blue popup">More</a>
              </div>
            </div>
          @endforeach
        </div>

        <span class="slider-nav-prev"><i class="fas fa-angle-left"></i></span>
        <span class="slider-nav-next"><i class="fas fa-angle-right"></i></span>

        @foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] as $n)
          <div id="testimony-popup-{{ $loop->index }}" class="mfp-hide w-25 p-3 mx-auto bg-white testimony-popup">
            <img src="{{ asset('images/anonym-profile.png') }}" alt="Testimony {{ $n }}"/>
            <h5>@lang('general.gender_testimony_'.$n)</h5>
            <div>@lang('general.content_testi_'.$n)</div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  @else
  <section id="home-testimony" class="home-testimony">
    <div class="container">
      <h3 class="section-header text-purple"><b>@lang('general.testimony')</b></h3>

      <div class="testimony--slider">
        <div id="slider-testimony">
          @foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] as $n)
            @php
              $name = __('general.name_testimony_'.$n);
              if(strpos($name, 'Mr') !== false){
                if($n % 2 == 0){
                  $profile = 'images/profile-1.png';
                } else if($n == 3){
                  $profile = 'images/profile-2.png';
                } else {
                  $profile = 'images/profile-3.png';
                }
              } else {
                if($n % 2 == 0){
                  $profile = 'images/profile-cw-1.png';
                } else if($n == 9){
                  $profile = 'images/profile-cw-2.png';
                } else {
                  $profile = 'images/profile-cw-3.png';
                }
              }
            @endphp
            <div class="slider-item">
              <img src="{{ asset($profile) }}" alt="Testimony {{ $n }}"/>
              <h5>@lang('general.name_testimony_'.$n)</h5>
              <h4>@lang('general.position_testimony_'.$n)</h4>
              <h3>@lang('general.work_testimony_'.$n)</h3>
              <div>
                <div
                style="overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-box-orient: vertical;
                -webkit-line-clamp: 5;
                line-height: 1.5em;        
                max-height: X*N; "
                >
                  @lang('general.content_testimony_'.$n)  
                </div>
                <a href="#testimony-popup-{{ $loop->index }}" class="text-blue popup">More</a>
              </div>
            </div>
          @endforeach
        </div>

        <span class="slider-nav-prev"><i class="fas fa-angle-left"></i></span>
        <span class="slider-nav-next"><i class="fas fa-angle-right"></i></span>

        @foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] as $n)
          <div id="testimony-popup-{{ $loop->index }}" class="mfp-hide w-25 p-3 mx-auto bg-white testimony-popup">
            <img src="{{ asset('images/anonym-profile.png') }}" alt="Testimony {{ $n }}"/>
            <h5>@lang('general.name_testimony_'.$n)</h5>
            <div>@lang('general.content_testimony_'.$n)</div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  @endif --}}

  {{-- <section id="home-clients" class="home-clients">
    <div class="container">
      <h3 class="section-header text-blue"><b>@lang('general.our_client')</b></h3>

      <p class="section-description text-center">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br> "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>

      <div class="client--logos">
        @php
          $logos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
        @endphp

        @foreach ($logos as $logo)
          <span title="Client {{ $logo }}"><img src="{{ asset('images/client-' . $logo . '.png') }}" alt="Client {{ $logo }}"/></span>
        @endforeach
      </div>
    </div>
  </section> --}}

  <section id="home-contact" class="home-contact">
    <div class="container">
      <h3 class="section-header text-blue"><b>@lang('general.contact_us')</b></h3>
      <p class="section-description text-center">@lang('general.info_contact')</p>

      <div class="row show-contact">
        <div class="col-6">
          <div class="tab-content">
            @php
              $contacts = [
                (object) ['image' => 'images/jakarta.png', 'title' => 'headquarters', 'address' => '19th Floor Midplaza 2 Jl. Jend. Sudirman Kav.10-11<br> Jakarta 10220 INDONESIA', 'phone' => '+62-21-572-7214', 'email' => 'inquiry@os-selnajaya.com', 'map' => '#', 'direction' => 'Direction'],
                (object) ['image' => 'images/cikarang.jpg', 'title' => 'cikarang', 'address' => 'Ruko Robson square BlokB7&B8,Jl.MH Thamrin<br>Lippo Cikarang, Desa Cibatu, kacamatan Cikarang Selatan, Kabupaten Bekasi 17530', 'phone' => '+62-21-897-2467', 'email' => '', 'map' => 'https://goo.gl/maps/HuJP1kCTZKrnJn698', 'direction' => 'Direction'],
                (object) ['image' => 'images/karawang-new.jpg', 'title' => 'karawang', 'address' => 'Sedana Golf Country Club Blok Kintamani Ruko No. 29 <br>Teluk Jambe, Karawang, Jawa Barat 41361', 'phone' => '+62-267-8458-741', 'email' => '', 'map' => 'https://goo.gl/maps/hDqhhi7rtS1JLvjq8', 'direction' => 'Direction'],
                (object) ['image' => 'images/JLMC.jpg', 'title' => 'bandung_jlmc', 'address' => 'Gedung Pratyaksa Lt.2, Jl. W. R. Supratman No.3<br>Bandung, Jawa Barat 40114', 'phone' => '+62-22-7233-185', 'email' => '', 'map' => 'https://goo.gl/maps/YYpb5hxuerGdNozk7', 'direction' => 'Direction'],
                (object) ['image' => 'images/lembang.jpg', 'title' => 'bandung(Lembang)', 'address' => 'Gedung Pratyaksa Lt.2, Jl. W. R. Supratman No.3<br>Bandung, Jawa Barat 40114', 'phone' => '+62-22-7233-185', 'email' => '', 'map' => 'https://goo.gl/maps/mYkX5j7g3RpuRJBn6', 'direction' => 'Direction'],
                (object) ['image' => 'images/surabaya.jpg', 'title' => 'surabaya', 'address' => 'SPAZIO BUILDING Lt. 6 Unit 625B Jl. Mayjend Yono Soewoyo Kavling 3<br>Surabaya, Jawa Timur 60226', 'phone' => '+62-31-6003-9580', 'email' => '', 'map' => 'https://goo.gl/maps/uZ9wzARXtEypMo5u5', 'direction' => 'Direction'],
                (object) ['image' => 'images/cibubur.jpg', 'title' => 'cibubur', 'address' => '&nbsp;', 'phone' => '', 'email' => '', 'map' => 'https://goo.gl/maps/ndrDbzCusxUVUHw98', 'direction' => 'Direction']
              ];
            @endphp

            @foreach($contacts as $c)
              <div id="contact-tab-{{ $loop->index }}" class="tab-pane fade {{ $loop->index === 0 ? 'show active' : '' }}" role="tabpanel">
                <div class="contact--detail">
                  @if ($c->image)
                <img src="{{ asset($c->image) }}" alt="Contact" width="{{ $c->title == 'Karawang' ? '70%' : 'auto' }}"/>
                  @endif

                  @if ($c->title)
                    <h4 class="text-uppercase">{{__('general.'.$c->title)}}</h4>
                  @endif

                  @if ($c->address)
                    <div class="detail-item">
                      <i class="fas fa-map-marker-alt"></i>
                      <a href="{{ $c->map }}" class="text-font"><span>{!! $c->address !!}</span></a>
                    </div>
                  @endif

                  @if ($c->phone)
                    <div class="detail-item">
                      <i class="fas fa-phone"></i>
                      <span>{{ $c->phone }}</span>
                    </div>
                  @endif

                  @if ($c->email)
                    <div class="detail-item">
                      <i class="fas fa-envelope"></i>
                      <span>{{ $c->email }}</span>
                    </div>
                  @endif

                  @if ($c->direction)
                    <div class="detail-item">
                      <i class="fas fa-directions"></i>
                      <span>Direction</span>
                    </div>
                  @endif
                </div>
              </div>
            @endforeach
          </div>
        </div>

        <div class="col-6">
          <div class="contact--navs nav" role="tablist">
            @foreach($contacts as $c)
              <a href="#contact-tab-{{ $loop->index }}" class="btn btn-blue btn-block {{ $loop->index === 0 ? 'active' : '' }}" data-toggle="tab" role="tab" aria-controls="contact-tab-{{ $loop->index }}" aria-selected="{{ $loop->index === 0 ? 'true' : 'false' }}">{{ __('general.'.$c->title) }}</a>
            @endforeach
            <a href="#" class="btn btn-blue btn-block" data-toggle="tab">@lang('general.other_countries')</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('layouts.footer')
@endsection
