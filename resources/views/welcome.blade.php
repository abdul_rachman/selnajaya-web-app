
<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <meta name="google-signin-client_id" content="411478525148-sqtvbnf8se9h5a7313fm0kr2a3tmao81.apps.googleusercontent.com">

    <title>@yield('title')</title>

    <link href="{{ asset('css/vendor.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">    
  </head>
  <body>

      @include('layouts.header')
      <section>
      
          <h1>
              Landing Page
          </h1>
      </section>
      
      
      @include('layouts.footer')
      
      <script src="{{ asset('js/vendor.js') }}"></script>
      <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>

