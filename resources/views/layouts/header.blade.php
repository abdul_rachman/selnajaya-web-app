<header id="master-header">
  <nav class="navbar navbar-expand-lg navbar-light bg-light" ">                 

      <nav class="header--nav">
        
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>                         

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-block">  
              <a href="http://selna-web.test"><img src="http://selna-web.test/images/logo.png" alt="Laravel Logo"></a>
            </li>
            <li class="nav-item">
              <a href="http://selna-web.test" class="nav-link active">Home</a> 
            </li>
            <li class="nav-item">
              <a href="http://selna-web.test/support" class="nav-link">Our Support</a>
            </li>
            <li class="nav-item">                            
              <a href="http://selna-web.test/jobs" class="nav-link">Jobs</a>
            </li>
            <li class="nav-item">              
              <a href="http://selna-web.test/career-advice" class="nav-link">Career Advice</a>
            </li>
            <li class="nav-item">              
              <a href="http://selna-web.test/news-event" class="nav-link">News/Event</a>
            </li>
            <li class="nav-item"> 
              <a href="http://selna-web.test/contact" class="nav-link">Contact Us</a>
            </li>
            <li class="nav-item"> 
              <a href="http://selna-web.test/internal-hiring" class="nav-link">Internal Hiring</a>
            </li>
            <li class="nav-item"> 
              <a href="http://selna-web.test/lang/jp" class="nav-link">JP</a>
            </li>
            <li class="nav-item"> 
              <a href="http://selna-web.test/lang/en" class="nav-link">EN</a>
            </li>
            <li class="nav-item">               
              <a href="http://selna-web.test/lang/id" class="nav-link">ID</a>
            </li>
          </ul>
          
        </div>
    </nav>                
  </div>
</header>

